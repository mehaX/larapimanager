<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInjectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('injections', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('from_class_id')->unsigned();
            $table->bigInteger('to_class_id')->unsigned();
            $table->timestamps();

            $table->foreign('from_class_id')->references('id')->on('class_names');
            $table->foreign('to_class_id')->references('id')->on('class_names');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('injections');
    }
}
