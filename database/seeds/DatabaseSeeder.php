<?php

use App\Type;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();
        Type::insert([
            [
                'id' => 1,
                'name' => 'None',
                'dir_name' => null,
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'id' => 2,
                'name' => 'Controller',
                'dir_name' => 'Controllers',
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'id' => 3,
                'name' => 'Model',
                'dir_name' => 'Models',
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'id' => 4,
                'name' => 'Service',
                'dir_name' => 'Services',
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'id' => 5,
                'name' => 'Repository',
                'dir_name' => 'Repositories',
                'created_at' => $now,
                'updated_at' => $now,
            ],
        ]);
    }
}
