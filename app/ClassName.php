<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ClassName
 * @package App
 * @property-read int id
 * @property int component_id
 * @property int type_id
 * @property string name
 * @property-read string created_at
 * @property-read string updated_at
 *
 * @property Component component
 * @property Type type
 * @method static Builder getQuery()
 */
class ClassName extends Model
{
    protected $table = 'class_names';

    protected $fillable = ['component_id', 'type_id', 'name'];

    public function component()
    {
        return $this->belongsTo(Component::class);
    }

    public function type()
    {
        return $this->belongsTo(Type::class);
    }
}
