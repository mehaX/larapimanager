<?php

namespace App\Http\Controllers;

use App\ClassName;
use App\Component;
use App\Http\Services\ComponentsService;
use App\Http\Services\DirScannerService;
use App\Type;

class HomeController extends Controller
{
    /**
     * @var DirScannerService
     */
    private $dirScannerService;

    /**
     * @var ComponentsService
     */
    private $componentsService;

    public function __construct(
        DirScannerService $dirScannerService,
        ComponentsService $componentsService
    )
    {
        $this->dirScannerService = $dirScannerService;
        $this->componentsService = $componentsService;
    }

    public function index()
    {
        $data['types'] = Type::all();
        $data['components'] = Component::all();
        $data['classes'] = ClassName::all();

        return view('home', $data);
    }

    public function scan()
    {
        $path = env('APP_PATH').'/api';

        $dirs = $this->dirScannerService->scanDir($path);

        return $dirs;
    }

    public function resetComponents()
    {
        $this->componentsService->resetComponents();
    }
}
