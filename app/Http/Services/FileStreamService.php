<?php


namespace App\Http\Services;

/**
 * Class FileStreamService
 * @package App\Http\Services
 */
class FileStreamService
{
    /**
     * @param string $file
     * @param bool $withRows
     * @return array|false|string
     */
    public function readFile($file, $withRows = false)
    {
        $fileContent = file_get_contents($file);

        if ($withRows) {
            return explode(PHP_EOL, $fileContent);
        }

        return $fileContent;
    }

    /**
     * @param string $file
     * @return string
     */
    public function readInlineFile($file)
    {
        $fileContent = $this->readFile($file);

        return str_replace(PHP_EOL, ' ', $fileContent);
    }
}
