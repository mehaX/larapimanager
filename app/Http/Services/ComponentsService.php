<?php


namespace App\Http\Services;


use App\ClassFunction;
use App\ClassName;
use App\Component;
use App\Type;

class ComponentsService
{
    private $classFunctionsRegex = '/(\/\*[^\/]+\*\/)?\s*public\s+function\s+([^\s\(]+)\s*\(([^\)]+)\)/g';

    /**
     * @var DirScannerService
     */
    private $dirScannerService;

    /**
     * @var string
     */
    private $componentsPath;

    /**
     * @var FileStreamService
     */
    private $fileStreamService;

    public function __construct(
        DirScannerService $dirScannerService,
        FileStreamService $fileStreamService
    )
    {
        $this->dirScannerService = $dirScannerService;
        $this->fileStreamService = $fileStreamService;

        $this->componentsPath = config('project.components');
    }

    private function resetData()
    {
        ClassFunction::getQuery()->delete();
        ClassName::getQuery()->delete();
        Component::getQuery()->delete();
    }

    public function resetComponents()
    {
        $this->resetData();
        $dirs = $this->dirScannerService->scanForDirectories($this->componentsPath);

        foreach ($dirs as $dir) {
            $component = new Component();
            $component->name = $dir;
            $component->save();
        }

        $this->saveClassNames();
    }

    /**
     * @param Type $type
     * @param Component $component
     */
    private function saveClassNameForComponent($type, $component)
    {
        $path = $this->componentsPath . DIRECTORY_SEPARATOR . $component->name . DIRECTORY_SEPARATOR . $type->dir_name;
        $files = $this->dirScannerService->scanForFiles($path);

        foreach ($files as $file) {
            $name = str_replace('.php', '', $file);

            $className = new ClassName();
            $className->component_id = $component->id;
            $className->type_id = $type->id;
            $className->name = $name;
            $className->save();

            $this->updateClassFunctions($path);
        }
    }

    public function saveClassNames()
    {
        $types = Type::all();
        $components = Component::all();

        foreach ($components as $component) {
            foreach ($types as $type) {
                if ($type->dir_name != null) {
                    $this->saveClassNameForComponent($type, $component);
                }
            }
        }
    }

    /**
     * @param string $path
     */
    private function updateClassFunctions(string $path)
    {
        $fileContent = $this->fileStreamService->readInlineFile($path);

        $matches = [];
        preg_match_all($this->classFunctionsRegex, $fileContent, $matches);

        dd($matches);
    }
}
