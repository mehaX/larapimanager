<?php


namespace App\Http\Services;

/**
 * Class DirScannerService
 * @package App\Http\Services
 */
class DirScannerService
{
    /**
     * @param string $path
     * @param string $type
     * @return string[]
     */
    public function scanDir($path, $type = 'all')
    {
        $list = scandir($path);

        $list = array_filter($list, function($item) {
            return $item != '.' && $item != '..';
        });

        switch($type) {
            case 'dirs':
                $list = array_filter($list, function($item) use($path) {
                    return is_dir($path.DIRECTORY_SEPARATOR.$item);
                });
                break;

            case 'files':
                $list = array_filter($list, function($item) use($path) {
                    return is_file($path.DIRECTORY_SEPARATOR.$item);
                });
                break;
        }

        return array_values($list);
    }

    /**
     * @param string $path
     * @return string[]
     */
    public function scanForFiles($path)
    {
        return $this->scanDir($path, 'files');
    }

    /**
     * @param string $path
     * @return string[]
     */
    public function scanForDirectories($path)
    {
        return $this->scanDir($path, 'dirs');
    }
}
