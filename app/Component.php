<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Component
 * @package App
 * @property-read int id
 * @property string name
 * @property string created_at
 * @property string updated_at
 * @method static Builder getQuery()
 */
class Component extends Model
{
    protected $table = 'components';

    protected $fillable = ['name'];
}
