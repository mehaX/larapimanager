<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property-read int id
 * @property-read string name
 * @property-read string created_at
 * @property-read string updated_at
 * @method static insert(array $array)
 */
class Type extends Model
{
    protected $table = 'types';

    protected $fillable = [];
}
