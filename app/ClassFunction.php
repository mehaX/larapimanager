<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ClassFunction
 * @package App
 * @property-read int id
 * @property string annotations
 * @property string function_name
 * @property string parameters
 * @property-read string created_at
 * @property-read string updated_at
 * @method static Builder getQuery()
 */
class ClassFunction extends Model
{
    protected $table = 'class_functions';

    protected $fillable = [
        'annotations', 'function_name', 'parameters',
    ];
}
