<?php

namespace App\Providers;

use App\Http\Services\DirScannerService;
use App\Http\Services\FileStreamService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(DirScannerService::class);
        $this->app->singleton(FileStreamService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
