<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Injection
 * @package App
 * @property-read int id
 * @property int from_class_id
 * @property int to_class_id
 * @property-read string created_at
 * @property-read string updated_at
 *
 * @property ClassName from_class
 * @property ClassName to_class
 */
class Injection extends Model
{
    protected $table = 'injections';

    protected $fillable = ['from_class_id', 'to_class_id'];

    public function from_class()
    {
        return $this->belongsTo(ClassName::class, 'from_class_id');
    }

    public function to_class()
    {
        return $this->belongsTo(ClassName::class, 'to_class_id');
    }
}
