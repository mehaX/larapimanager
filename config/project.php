<?php
return [
    'components' => env('APP_PATH').'/api',
    'routes' => [
        'public' => 'routes_public.php',
        'private' => 'routes.php',
        'protected' => 'routes_protected.php',
    ],
];
